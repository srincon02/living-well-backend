package com.wax.wax.Dominio.servicios;
import com.wax.wax.Infraestructura.output.persistence.entidades.usuarioresidenteEntidad;
import com.wax.wax.Infraestructura.output.persistence.repositorio.AuditoriaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wax.wax.Infraestructura.output.persistence.repositorio.usuarioresidenteRepositorio;
import com.wax.wax.Infraestructura.output.persistence.entidades.AuditoriaEntidad;
import java.time.LocalDate;


@Service
public class AuditoriaServicios {

    @Autowired
    private AuditoriaRepositorio auditoriaRepositorio;

    @Autowired
    private usuarioresidenteRepositorio usuarioresidenteRepositorio;

    public void guardarAuditoria(Long idUsuario,String accion) {
        AuditoriaEntidad auditoria = new AuditoriaEntidad();
        usuarioresidenteEntidad usuario = usuarioresidenteRepositorio.findById(idUsuario).orElseThrow();
        auditoria.setId(usuario);
        auditoria.setAccion(accion);
        auditoria.setFecha(LocalDate.now());

        auditoriaRepositorio.save(auditoria);
    }
}