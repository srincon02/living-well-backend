package com.wax.wax.Dominio.servicios;

import com.wax.wax.Infraestructura.output.persistence.entidades.correspondenciaEntidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.wax.wax.Infraestructura.output.persistence.repositorio.correspondenciaRepositorio;


@Service
public class correspondenciaService {
    @Autowired
    private correspondenciaRepositorio correspondenciaRepositorio;

    public List<correspondenciaEntidad> buscarTodas() {
        return correspondenciaRepositorio.findAll();
    }

    public correspondenciaEntidad buscarPorId(Integer id) {
        return correspondenciaRepositorio.findById(Long.valueOf(id)).orElse(null);
    }

    public correspondenciaEntidad guardar(correspondenciaEntidad correspondencia) {
        return correspondenciaRepositorio.save(correspondencia);
    }

    public void eliminar(Integer id) {
        correspondenciaRepositorio.deleteById(Long.valueOf(id));
    }
}