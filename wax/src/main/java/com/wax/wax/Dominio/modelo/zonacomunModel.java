package com.wax.wax.Dominio.modelo;

import lombok.*;

import java.util.LinkedHashSet;
import java.util.Set;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class zonacomunModel {


    private Integer id;
    private String zonaComun;
    private Set<reservazonaModel> reservaZonas = new LinkedHashSet<>();
}
