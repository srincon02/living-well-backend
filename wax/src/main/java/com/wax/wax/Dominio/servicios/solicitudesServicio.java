package com.wax.wax.Dominio.servicios;


import com.wax.wax.Infraestructura.output.persistence.entidades.solicitudesEntidad;
import com.wax.wax.Infraestructura.output.persistence.entidades.usuarioresidenteEntidad;
import com.wax.wax.Infraestructura.output.persistence.repositorio.solicitudesRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class solicitudesServicio {

    private final solicitudesRepositorio solicitudesRepositorio;

    @Autowired
    public solicitudesServicio(solicitudesRepositorio solicitudesRepositorio) {
        this.solicitudesRepositorio = solicitudesRepositorio;
    }

    @Autowired
    private EmailService emailService;
    public solicitudesEntidad crearSolicitud(solicitudesEntidad solicitud, String subject) {

        LocalDate fechaActual = LocalDate.now();

        // Asignar la fecha actual a la columna fechaRad
        solicitud.setFechaRad(fechaActual);

        // Obtener el objeto usuarioresidenteEntidad relacionado
        usuarioresidenteEntidad usuarioResidente = solicitud.getFk();

        // Obtener el correo electrónico del usuario residente
        String correoElectronico = usuarioResidente.getCorreoElectronico();

        // Enviar correo electrónico
        emailService.sendEmail(usuarioResidente.getId(), subject);

        // Guardar la solicitud en el repositorio para obtener el ID generado automáticamente
        solicitudesEntidad nuevaSolicitud = solicitudesRepositorio.save(solicitud);

        // Generar el número de radicado en el formato "RAD-{id}"
        String radicado = "RAD-" + nuevaSolicitud.getId();
        nuevaSolicitud.setRadicado(radicado);

        // Guardar la solicitud actualizada con el radicado en el repositorio
        return solicitudesRepositorio.save(nuevaSolicitud);
    }

    public List<solicitudesEntidad> obtenerTodasLasSolicitudes() {
        return solicitudesRepositorio.findAll();
    }

    public solicitudesEntidad obtenerSolicitudPorId(Long id) {
        Optional<solicitudesEntidad> solicitud = solicitudesRepositorio.findById(id);
        return solicitud.orElse(null);
    }

    public solicitudesEntidad actualizarSolicitud(Long id, solicitudesEntidad solicitud) {
        Optional<solicitudesEntidad> solicitudExistente = solicitudesRepositorio.findById(id);

        if (solicitudExistente.isPresent()) {
            solicitudesEntidad solicitudActualizada = solicitudExistente.get();
            solicitudActualizada.setNum(solicitud.getNum());
            solicitudActualizada.setFechaRad(solicitud.getFechaRad());
            solicitudActualizada.setFechaSol(solicitud.getFechaSol());
            solicitudActualizada.setRadicado(solicitud.getRadicado());
            solicitudActualizada.setDescripcion(solicitud.getDescripcion());
            return solicitudesRepositorio.save(solicitudActualizada);
        } else {
            return null;
        }
    }



    public void eliminarSolicitud(Long id) {
        solicitudesRepositorio.deleteById(id);
    }
}