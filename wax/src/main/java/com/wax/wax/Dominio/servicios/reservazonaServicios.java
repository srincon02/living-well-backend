package com.wax.wax.Dominio.servicios;

import com.wax.wax.Dominio.excepciones.ReservaNoDisponibleExcepcion;
import com.wax.wax.Dominio.excepciones.fechaInvalidaExcepcion;
import com.wax.wax.Infraestructura.output.persistence.entidades.reservazonaEntidad;
import com.wax.wax.Infraestructura.output.persistence.repositorio.reservazonaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class reservazonaServicios {


    @Autowired
    private reservazonaRepositorio reservazonaRepositorio;

    public List<reservazonaEntidad> obtenerTodasLasReservas() {
        return reservazonaRepositorio.findAll();
    }

    public reservazonaEntidad crearReserva(reservazonaEntidad reserva) {
        LocalDate fechaActual = LocalDate.now();
        LocalDate fechaReserva = reserva.getFecha();
        LocalTime FechaFin = reserva.getFin();
        LocalTime FechaInicio = reserva.getInicio();
        Integer Zona = reserva.getFkZc().getId();

//        List<reservazonaEntidad> reservas = reservazonaRepositorio.findByFkZcAndFecha(Zona, fechaReserva);
//
//        for (reservazonaEntidad reservaExistente : reservas) {
//            if (FechaInicio.isBefore(reservaExistente.getFin())
//                    && FechaFin.isAfter(reservaExistente.getInicio())) {
//                throw new ReservaNoDisponibleExcepcion("La zona común no está disponible para la fecha y hora seleccionadas");
//            }
//        }


        if (fechaReserva.isBefore(fechaActual)) {
            throw new fechaInvalidaExcepcion("No se puede crear una reserva en una fecha anterior a la fecha actual");
        }

        if(FechaFin.isBefore(FechaInicio)){
            throw new fechaInvalidaExcepcion("La fecha de fin no puede ser menor que la fecha de inicio");
        }

        return reservazonaRepositorio.save(reserva);
    }


    public Optional<reservazonaEntidad> obtenerReservaPorId(Long id) { return reservazonaRepositorio.findById(id);
    }

    public reservazonaEntidad actualizarReserva(Long id, reservazonaEntidad reserva) {
        Optional<reservazonaEntidad> reservaExistente = reservazonaRepositorio.findById(id);

        if (reservaExistente.isPresent()) {
            reservazonaEntidad reservaActualizada = reservaExistente.get();
            reservaActualizada.setFkidentificacion(reserva.getFkidentificacion());
            reservaActualizada.setFkTr(reserva.getFkTr());
            reservaActualizada.setFkZc(reserva.getFkZc());
            reservaActualizada.setFecha(reserva.getFecha());
            reservaActualizada.setTiempoReserva(reserva.getTiempoReserva());
            reservaActualizada.setInicio(reserva.getInicio());
            reservaActualizada.setFin(reserva.getFin());
            return reservazonaRepositorio.save(reservaActualizada);
        } else {
            return null;
        }
    }

    public void eliminarReserva(Long id) {
        reservazonaRepositorio.deleteById(id);
    }
}