package com.wax.wax.Dominio.servicios;

import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import com.wax.wax.Infraestructura.output.persistence.entidades.usuarioresidenteEntidad;
import com.wax.wax.Infraestructura.output.persistence.repositorio.usuarioresidenteRepositorio;

import java.io.File;
import java.util.Optional;
import java.util.Properties;

@Service
public class EmailService {

    private final usuarioresidenteRepositorio usuarioresidenteRepositorio;

    public EmailService(usuarioresidenteRepositorio usuarioresidenteRepositorio) {
        this.usuarioresidenteRepositorio = usuarioresidenteRepositorio;
    }
    @Bean("javaMailSender")
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost("smtp.gmail.com");
        sender.setPort(587);
        sender.setUsername("ingescato@gmail.com");
        sender.setPassword("vthfntkichltwyiz");

        Properties props = sender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return sender;
    }


    public String sendEmail(Integer usuarioResidenteId, String subject) {
        MimeMessage message = javaMailSender().createMimeMessage();

        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");

            messageHelper.setFrom("ingescato@gmail.com");

            // Buscar el usuario residente en la base de datos
            Optional<usuarioresidenteEntidad> usuarioResidenteOptional = usuarioresidenteRepositorio.findById(Long.valueOf(usuarioResidenteId));
            if (usuarioResidenteOptional.isPresent()) {
                usuarioresidenteEntidad usuarioResidente = usuarioResidenteOptional.get();

                // Obtener el correo electrónico del usuario residente
                String correoElectronico = usuarioResidente.getCorreoElectronico();

                // Establecer el correo electrónico del destinatario
                messageHelper.setTo(correoElectronico);
            } else {
                return "Mail sent failed: User not found";
            }

            messageHelper.setSubject(subject);

            // Crear el contenido HTML del correo electrónico
            String htmlContent = "<div style='text-align: center;'>";
            htmlContent += "<img src='cid:image'>";
            htmlContent += "</div>";

            // Agregar el contenido HTML al correo electrónico
            messageHelper.setText(htmlContent, true);

            // Ruta de la imagen a incluir en el correo
            String imagePath = "C:\\Users\\steve\\OneDrive\\Escritorio\\Wax Final\\wax\\src\\main\\resources\\static\\img\\visita.jpg";

            // Cargar la imagen desde el archivo
            FileSystemResource image = new FileSystemResource(new File(imagePath));

            // Agregar la imagen como una parte del correo con un CID
            messageHelper.addInline("image", image);

            // Enviar el correo electrónico
            javaMailSender().send(message);

            return "Mail sent successfully";
        } catch (Exception e) {
            return "Mail sent failed: " + e.getMessage();
        }
    }
    public String sendEmailwithAttachment() {
        try {
            MimeMessage message = javaMailSender().createMimeMessage();

            MimeMessageHelper messageHelper =
                    new MimeMessageHelper(message, true);

            messageHelper.setFrom("ingescato@gmail.com");
            messageHelper.setTo("lhpoveda18@ucatolica.edu.co");
            messageHelper.setSubject("Test Subject");
            messageHelper.setText("Test Body");

            File file = new File("L:\\Leyder\\Desktop\\Wax Definitivo\\Leyder\\wax\\src\\main\\resources\\static\\img\\undraw_posting_photo.svg");

            messageHelper.addAttachment(file.getName(), file);

            javaMailSender().send(message);

            return "Mail sent successfully";

        } catch (Exception e) {
            return "Mail sent failed";
        }
    }
}