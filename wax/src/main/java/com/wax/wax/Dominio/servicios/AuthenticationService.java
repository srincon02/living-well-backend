package com.wax.wax.Dominio.servicios;


import com.wax.wax.Infraestructura.adapters.input.controlador.JwtAuthenticationResponse;
import com.wax.wax.Infraestructura.request.SignUpRequest;
import com.wax.wax.Infraestructura.request.SigninRequest;

public interface AuthenticationService {
    JwtAuthenticationResponse signup(SignUpRequest request);

    JwtAuthenticationResponse signin(SigninRequest request);
}