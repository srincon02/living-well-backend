package com.wax.wax.Dominio.excepciones;

public class ReservaNoDisponibleExcepcion extends RuntimeException{

    public ReservaNoDisponibleExcepcion(String mensaje) {
        super(mensaje);
    }
}
