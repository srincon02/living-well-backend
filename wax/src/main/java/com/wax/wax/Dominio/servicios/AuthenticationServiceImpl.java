package com.wax.wax.Dominio.servicios;

import com.wax.wax.Infraestructura.adapters.input.controlador.JwtAuthenticationResponse;
import com.wax.wax.Infraestructura.output.persistence.entidades.Role;
import com.wax.wax.Infraestructura.output.persistence.entidades.User;
import com.wax.wax.Infraestructura.output.persistence.entidades.usuarioresidenteEntidad;
import com.wax.wax.Infraestructura.output.persistence.repositorio.UserRepository;
import com.wax.wax.Infraestructura.request.SignUpRequest;
import com.wax.wax.Infraestructura.request.SigninRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import com.wax.wax.Infraestructura.output.persistence.repositorio.usuarioresidenteRepositorio;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final usuarioresidenteRepositorio usuarioresidenteRepositorio;
    @Override
    public JwtAuthenticationResponse signup(SignUpRequest request) {
        usuarioresidenteEntidad usuario = usuarioresidenteRepositorio.findByCorreoElectronico(request.getEmail());
        // Obtener el tipo de usuario desde la tabla usuarioresidente
        String tipoUsuario = String.valueOf(usuario.getFkTusr().getNTUsuario()); // Ajusta esto según el nombre de la columna que almacena el tipo de usuario

        var user = User.builder().firstName(request.getFirstName()).lastName(request.getLastName())
                .email(request.getEmail()).password(passwordEncoder.encode(request.getPassword()))
                .role(Role.valueOf(tipoUsuario)).build();
        userRepository.save(user);
        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }

    @Override
    public JwtAuthenticationResponse signin(SigninRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password"));
        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }
}