package com.wax.wax.Aplicacion.inputports;

import com.wax.wax.Dominio.modelo.VisitanteModel;


public interface CrearVisitanteCasoUso {

    void create(VisitanteModel visitanteModel);
}
