package com.wax.wax.Infraestructura.output.persistence.entidades;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import com.wax.wax.Infraestructura.output.persistence.entidades.solicitudesEntidad;
import lombok.*;

import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "tiposolicitud")
public class tiposolicitudEntidad {
    @Id
    @Column(name = "pk_tiposolicitud", nullable = false)
    private Long id;

    @Column(name = "tipo_solicitud", nullable = false, length = Integer.MAX_VALUE)
    private String tipoSolicitud;

    @OneToMany(mappedBy = "Tiposolicitud")
    private Set<solicitudesEntidad> solicitudes = new LinkedHashSet<>();

}