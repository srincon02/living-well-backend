package com.wax.wax.Infraestructura.output.persistence.repositorio;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.wax.wax.Infraestructura.output.persistence.entidades.serviciosVariosEntidad;

@Repository
public interface ServiciosVariosRepositorio extends JpaRepository<serviciosVariosEntidad, Integer> {
    // Puedes agregar consultas personalizadas si las necesitas
}
