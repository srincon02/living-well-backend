package com.wax.wax.Infraestructura.adapters.input.controlador;

import com.wax.wax.Infraestructura.output.persistence.entidades.correspondenciaEntidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.wax.wax.Dominio.servicios.correspondenciaService;
import java.util.List;

@CrossOrigin(origins="http://waxv5-env.eba-kbpgxkzk.us-east-1.elasticbeanstalk.com/")
@RestController
@RequestMapping("/correspondencias")
public class correspondenciaControlador {
    @Autowired
    private correspondenciaService correspondenciaServicio;

    @GetMapping("/")
    public List<correspondenciaEntidad> buscarTodas() {
        return correspondenciaServicio.buscarTodas();
    }

    @GetMapping("/{id}")
    public correspondenciaEntidad buscarPorId(@PathVariable Integer id) {
        return correspondenciaServicio.buscarPorId(id);
    }

    @PostMapping("/")
    public correspondenciaEntidad guardar(@RequestBody correspondenciaEntidad correspondencia) {
        return correspondenciaServicio.guardar(correspondencia);
    }

    @DeleteMapping("/{id}")
    public void eliminar(@PathVariable Integer id) {
        correspondenciaServicio.eliminar(id);
    }
}