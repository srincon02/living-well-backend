package com.wax.wax.Infraestructura.output.persistence.repositorio;

import java.time.LocalDate;
import java.util.Optional;

import com.wax.wax.Infraestructura.output.persistence.entidades.User;
import com.wax.wax.Infraestructura.output.persistence.entidades.VisitanteEntidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);
    Optional<User> findById(Integer identificacion);
    boolean existsByIdentificacion(Integer usuarioresidenteEntidad);
}