package com.wax.wax.Infraestructura.output.persistence.repositorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.wax.wax.Infraestructura.output.persistence.entidades.reservazonaEntidad;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface reservazonaRepositorio extends JpaRepository<reservazonaEntidad, Long> {
    List<reservazonaEntidad> findByFkZcAndFecha(Integer fkZc, LocalDate fecha);
}
