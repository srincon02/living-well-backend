package com.wax.wax.Infraestructura.output.persistence.repositorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.wax.wax.Infraestructura.output.persistence.entidades.AuditoriaEntidad;

@Repository
public interface AuditoriaRepositorio extends JpaRepository<AuditoriaEntidad, Long>{
}
