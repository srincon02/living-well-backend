package com.wax.wax.Infraestructura.adapters.input.controlador;


import com.wax.wax.Dominio.servicios.AuthenticationService;
import com.wax.wax.Infraestructura.output.persistence.entidades.Role;
import com.wax.wax.Infraestructura.output.persistence.entidades.User;
import com.wax.wax.Infraestructura.output.persistence.entidades.usuarioresidenteEntidad;
import com.wax.wax.Infraestructura.output.persistence.repositorio.UserRepository;
import com.wax.wax.Infraestructura.request.SignUpRequest;
import com.wax.wax.Infraestructura.request.SigninRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wax.wax.Infraestructura.adapters.input.controlador.JwtAuthenticationResponse;
import com.wax.wax.Infraestructura.output.persistence.repositorio.usuarioresidenteRepositorio;

import lombok.RequiredArgsConstructor;

import java.util.Optional;


@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;
    private final UserRepository userRepository;
    private final usuarioresidenteRepositorio usuarioresidenteRepositorio;

    @PostMapping("/signup")
    public ResponseEntity<JwtAuthenticationResponse> signup(@RequestBody SignUpRequest request) {
        // Verificar si el usuario ya existe en la tabla usuarioresidente
        usuarioresidenteEntidad usuario = usuarioresidenteRepositorio.findByCorreoElectronico(request.getEmail());
        if (usuario == null) {
            // El usuario no existe en la tabla usuarioresidente, retornar un mensaje de error
            return ResponseEntity.badRequest().body(new JwtAuthenticationResponse("El usuario no está creado"));
        }

        // Obtener el tipo de usuario desde la tabla usuarioresidente
        String tipoUsuario = String.valueOf(usuario.getFkTusr().getNTUsuario()); // Ajusta esto según el nombre de la columna que almacena el tipo de usuario


        // El usuario existe en la tabla usuarioresidente, continuar con el registro
        // Verificar si el usuario ya existe en la tabla de usuarios
        Optional<User> existingUser = userRepository.findByEmail(request.getEmail());
        if (existingUser.isPresent()) {
            // El usuario ya existe en la tabla de usuarios, retornar un mensaje de error
            return ResponseEntity.badRequest().body(new JwtAuthenticationResponse("El usuario ya existe"));
        }

        // Crear un nuevo User basado en los datos proporcionados en la solicitud
        User newUser = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .password(request.getPassword())
                .role(Role.valueOf(tipoUsuario))  // Establecer el rol adecuado según tus requisitos
                .build();

        // Llamar al método signup del servicio AuthenticationService
        return ResponseEntity.ok(authenticationService.signup(request));
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> signin(@RequestBody SigninRequest request) {
        return ResponseEntity.ok(authenticationService.signin(request));
    }
}