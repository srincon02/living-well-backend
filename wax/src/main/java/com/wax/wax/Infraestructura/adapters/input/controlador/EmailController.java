package com.wax.wax.Infraestructura.adapters.input.controlador;

import com.wax.wax.Dominio.servicios.EmailService;
import com.wax.wax.Infraestructura.output.persistence.entidades.usuarioresidenteEntidad;
import com.wax.wax.Infraestructura.output.persistence.repositorio.usuarioresidenteRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class EmailController {

    @Autowired
    private usuarioresidenteRepositorio usuarioResidenteRepositorio;
    EmailService emailService;

    @PostMapping("/sendEmail")
    public String sendEmail(@RequestParam Long usuarioResidenteId, @RequestParam String subject) {
        Optional<usuarioresidenteEntidad> usuarioResidenteOptional = usuarioResidenteRepositorio.findById(usuarioResidenteId);
        if (usuarioResidenteOptional.isPresent()) {
            usuarioresidenteEntidad usuarioResidente = usuarioResidenteOptional.get();
            return emailService.sendEmail(usuarioResidente.getId(), subject);
        } else {
            return "Mail sent failed: User not found";
        }
    }

    @GetMapping("/sendEmailwithAttachment")
    public String sendEmailwithAttachment() {
        return emailService.sendEmailwithAttachment();
    }

}