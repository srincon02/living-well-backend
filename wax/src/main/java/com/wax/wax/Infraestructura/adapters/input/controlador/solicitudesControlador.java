package com.wax.wax.Infraestructura.adapters.input.controlador;

import com.wax.wax.Dominio.excepciones.fechaInvalidaExcepcion;
import com.wax.wax.Dominio.servicios.solicitudesServicio;
import com.wax.wax.Infraestructura.output.persistence.entidades.solicitudesEntidad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/solicitudes")
public class solicitudesControlador {

    private final solicitudesServicio solicitudesServicio;

    @Autowired
    public solicitudesControlador(solicitudesServicio solicitudesServicio) {
        this.solicitudesServicio = solicitudesServicio;
    }

    @PostMapping
    public ResponseEntity<?> crearSolicitud(@RequestBody solicitudesEntidad solicitud) {
        try {
            // Verificar el rol de la persona logueada
            if (hasRole("Propietario") || hasRole("Administrador")) {
                solicitudesEntidad nuevaSolicitud = solicitudesServicio.crearSolicitud(solicitud, "Solicitud de mantenimiento creada");
                return ResponseEntity.status(HttpStatus.CREATED).body(nuevaSolicitud);
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
            }
        } catch (fechaInvalidaExcepcion e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/")
    public List<solicitudesEntidad> obtenerTodasLasReservas() {
        // Verificar el rol de la persona logueada
        if (hasRole("Propietario") || hasRole("Administrador")) {
            List<solicitudesEntidad> solicitudes = solicitudesServicio.obtenerTodasLasSolicitudes();
            return solicitudes;
        } else {
            return (List<solicitudesEntidad>) ResponseEntity.status(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<solicitudesEntidad> obtenerSolicitudPorId(@PathVariable("id") Long id) {
        solicitudesEntidad solicitud = solicitudesServicio.obtenerSolicitudPorId(id);
        if (solicitud != null) {
            return ResponseEntity.ok(solicitud);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<solicitudesEntidad> actualizarSolicitud(
            @PathVariable("id") Long id,
            @RequestBody solicitudesEntidad solicitud) {
        // Verificar el rol de la persona logueada
        if (hasRole("Propietario") || hasRole("Administrador")) {
            solicitudesEntidad solicitudActualizada = solicitudesServicio.actualizarSolicitud(id, solicitud);
            if (solicitudActualizada != null) {
                return ResponseEntity.ok(solicitudActualizada);
            } else {
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarSolicitud(@PathVariable("id") Long id) {
        // Verificar el rol de la persona logueada
        if (hasRole("Propietario") || hasRole("Administrador")) {
            solicitudesServicio.eliminarSolicitud(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
    }

    private boolean hasRole(String role) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream().anyMatch(authority -> authority.getAuthority().equals(role));
    }
}
