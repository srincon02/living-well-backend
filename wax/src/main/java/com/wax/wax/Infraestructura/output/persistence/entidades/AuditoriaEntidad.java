package com.wax.wax.Infraestructura.output.persistence.entidades;
import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "auditorialog")
public class AuditoriaEntidad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_log", nullable = false)
    private Integer idlog;

    @jakarta.validation.constraints.NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "identificacion", nullable = false)
    private usuarioresidenteEntidad id;

    @jakarta.validation.constraints.NotNull
    @Column(name = "fecha_accion", nullable = false)
    private LocalDate fecha;

    @jakarta.validation.constraints.NotNull
    @Column(name = "tipo_accion", nullable = false)
    private String accion;
}