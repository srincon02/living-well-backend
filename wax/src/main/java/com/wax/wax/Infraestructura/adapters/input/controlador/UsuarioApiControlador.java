package com.wax.wax.Infraestructura.adapters.input.controlador;

import com.wax.wax.Infraestructura.output.persistence.entidades.UsuarioApi;
import com.wax.wax.Infraestructura.output.persistence.repositorio.UsuarioApiRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioApiControlador {
    @Autowired
    private UsuarioApiRepositorio usuarioRepository;

    @PostMapping("/registro")
    public void registrarUsuario(@RequestBody UsuarioApi usuario) {
        usuarioRepository.save(usuario);
    }
}