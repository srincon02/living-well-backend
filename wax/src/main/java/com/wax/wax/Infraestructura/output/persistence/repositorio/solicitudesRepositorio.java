package com.wax.wax.Infraestructura.output.persistence.repositorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.wax.wax.Infraestructura.output.persistence.entidades.solicitudesEntidad;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface solicitudesRepositorio extends JpaRepository<solicitudesEntidad, Long> {

}