package com.wax.wax.Infraestructura.output.persistence.repositorio;

import com.wax.wax.Infraestructura.output.persistence.entidades.UsuarioApi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioApiRepositorio extends JpaRepository<UsuarioApi, Long> {

}