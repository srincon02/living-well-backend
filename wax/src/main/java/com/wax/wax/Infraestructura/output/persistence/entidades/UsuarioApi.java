package com.wax.wax.Infraestructura.output.persistence.entidades;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "usuarioapi")
public class UsuarioApi {
    @Id
    @Column(name = "pk_userapi", nullable = false)
    private Integer id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_id", nullable = false)
    private usuarioresidenteEntidad fk;

    @NotNull
    @Column(name = "nombre", nullable = false, length = Integer.MAX_VALUE)
    private String nombre;

    @NotNull
    @Column(name = "apellido", nullable = false, length = Integer.MAX_VALUE)
    private String apellido;

    @NotNull
    @Column(name = "email", nullable = false, length = Integer.MAX_VALUE)
    private String email;

    @NotNull
    @Column(name = "contrasena", nullable = false, length = Integer.MAX_VALUE)
    private String contrasena;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_tusr", nullable = false)
    private tipousuarioEntidad fkTusr;

}