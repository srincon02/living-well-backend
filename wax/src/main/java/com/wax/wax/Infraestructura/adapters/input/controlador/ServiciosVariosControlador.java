package com.wax.wax.Infraestructura.adapters.input.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.wax.wax.Dominio.servicios.EmailService;
import com.wax.wax.Dominio.servicios.ServiciosVariosService;
import com.wax.wax.Infraestructura.output.persistence.entidades.serviciosVariosEntidad;
import com.wax.wax.Infraestructura.output.persistence.entidades.usuarioresidenteEntidad;

import java.util.List;

@RestController
@RequestMapping("/servicios")

    public class ServiciosVariosControlador {

    @Autowired
    private ServiciosVariosService serviciosVariosService;

    @Autowired
    private EmailService emailService;
    @PostMapping
    public ResponseEntity<?> crearSolicitud(@RequestBody serviciosVariosEntidad servicioVario) {
        // Obtener el objeto usuarioresidenteEntidad relacionado
        usuarioresidenteEntidad usuarioResidente = servicioVario.getFk();

        // Obtener el correo electrónico del usuario residente
        String correoElectronico = usuarioResidente.getCorreoElectronico();

        // Enviar correo electrónico
        emailService.sendEmail(usuarioResidente.getId(), "Servicios varios registrados");

        // Crear la nueva solicitud
        serviciosVariosEntidad nuevaSolicitud = serviciosVariosService.create(servicioVario,"Servicios varios registrados");

        return ResponseEntity.status(HttpStatus.CREATED).body(nuevaSolicitud);
    }

    @GetMapping("/")
    public List<serviciosVariosEntidad> obtenerTodosLasServicios() {
        List<serviciosVariosEntidad> solicitudes = serviciosVariosService.obtenerTodosLasServicios();
        return solicitudes;
    }

    @GetMapping("/{id}")
    public ResponseEntity<serviciosVariosEntidad> obtenerSolicitudPorId(@PathVariable("id") Long id) {
        serviciosVariosEntidad solicitud = serviciosVariosService.obtenerSolicitudPorId(id);
        if (solicitud != null) {
            return ResponseEntity.ok(solicitud);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarSolicitud(@PathVariable("id") Long id) {
        serviciosVariosService.eliminarSolicitud(id);
        return ResponseEntity.noContent().build();
    }
}