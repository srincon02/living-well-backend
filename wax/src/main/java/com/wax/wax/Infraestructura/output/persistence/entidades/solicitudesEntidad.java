package com.wax.wax.Infraestructura.output.persistence.entidades;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "solicitudes")
public class solicitudesEntidad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_solicitud", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_id", nullable = false)
    private usuarioresidenteEntidad fk;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_tiposolicitud", nullable = false)
    private tiposolicitudEntidad Tiposolicitud;

    @NotNull
    @Column(name = "num", columnDefinition = "int4[] not null")
    private Integer num;

    @NotNull
    @Column(name = "fecha_rad", columnDefinition = "date[](13) not null")
    private LocalDate fechaRad;

    @NotNull
    @Column(name = "fecha_sol", columnDefinition = "date[](13)")
    private Date fechaSol;

    @NotNull
    @Column(name = "radicado", columnDefinition = "varchar[] not null")
    private String radicado;

    @NotNull
    @Column(name = "descripcion", columnDefinition = "varchar[] not null")
    private String descripcion;
}