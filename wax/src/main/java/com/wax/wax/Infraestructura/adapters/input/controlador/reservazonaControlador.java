package com.wax.wax.Infraestructura.adapters.input.controlador;
import com.wax.wax.Dominio.excepciones.fechaInvalidaExcepcion;
import com.wax.wax.Dominio.servicios.AuditoriaServicios;
import com.wax.wax.Infraestructura.output.persistence.entidades.reservazonaEntidad;
import com.wax.wax.Infraestructura.output.persistence.repositorio.reservazonaRepositorio;
import com.wax.wax.Dominio.servicios.reservazonaServicios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/reserva")
public class reservazonaControlador {
    @Autowired
    private reservazonaServicios reservazonaServicios;

    @Autowired
    private reservazonaRepositorio reservazonaRepositorio;

    @GetMapping("/")
    public List<reservazonaEntidad> obtenerTodasLasReservas() {
        List<reservazonaEntidad> reservas = reservazonaServicios.obtenerTodasLasReservas();
        for (reservazonaEntidad reserva : reservas) {
            reserva.setInicio(reserva.getInicio());
            reserva.setFin(reserva.getFin());
        }
        return reservas;
    }



    @GetMapping("/{id}")
    public reservazonaEntidad obtenerReservaPorId(@PathVariable Long id) { return reservazonaServicios.obtenerReservaPorId(id).orElseThrow(() -> new NoSuchElementException("Reserva no encontrada con el id: " + id));
    }

    @Autowired
    private AuditoriaServicios auditoriaServicios;

    @PostMapping("/")
    public ResponseEntity<?> crearReserva(@RequestBody reservazonaEntidad reserva) {
        try {
            Integer idUsuario = reserva.getFkidentificacion().getId();
            reservazonaEntidad nuevaReserva = reservazonaServicios.crearReserva(reserva);
            auditoriaServicios.guardarAuditoria(Long.valueOf(idUsuario),"CREAR_RESERVA");
            return new ResponseEntity<>("Reserva creada exitosamente con id " + nuevaReserva.getId(), HttpStatus.CREATED);
        } catch (fechaInvalidaExcepcion e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public reservazonaEntidad actualizarReserva(@PathVariable Long id, @RequestBody reservazonaEntidad reserva) {
        return reservazonaServicios.actualizarReserva(id, reserva);
    }

    @DeleteMapping("/{id}")
    public void eliminarReserva(@PathVariable Long id) {
        reservazonaServicios.eliminarReserva(id);
    }

}