package wax.wax.Aplicacion.inputports;

import wax.wax.Dominio.modelo.VisitanteModel;


public interface CrearVisitanteCasoUso {

    void create(VisitanteModel visitanteModel);
}
