package wax.wax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaxApplication {

	public static void main(String[] args) {
		SpringApplication.run(WaxApplication.class, args);
	}

}
