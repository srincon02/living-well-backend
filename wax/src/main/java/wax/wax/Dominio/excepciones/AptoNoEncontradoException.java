package wax.wax.Dominio.excepciones;

public class AptoNoEncontradoException extends RuntimeException {
    public AptoNoEncontradoException(String mensaje) {
        super(mensaje);
    }
}