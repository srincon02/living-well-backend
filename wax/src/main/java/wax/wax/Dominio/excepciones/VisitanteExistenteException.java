package wax.wax.Dominio.excepciones;

public class VisitanteExistenteException extends RuntimeException {
    public VisitanteExistenteException(String message) {
        super(message);
    }
}