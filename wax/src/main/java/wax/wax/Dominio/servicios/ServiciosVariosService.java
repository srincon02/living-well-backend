package wax.wax.Dominio.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wax.wax.Infraestructura.output.persistence.entidades.serviciosVariosEntidad;
import wax.wax.Infraestructura.output.persistence.entidades.usuarioresidenteEntidad;
import wax.wax.Infraestructura.output.persistence.repositorio.ServiciosVariosRepositorio;


import java.util.List;
import java.util.Optional;

@Service
public class ServiciosVariosService {

    @Autowired
    private ServiciosVariosRepositorio serviciosVariosRepositorio;

    @Autowired
    private EmailService emailService; // Inyecta el servicio de correo electrónico


    public List<serviciosVariosEntidad> obtenerTodosLasServicios() {
        return serviciosVariosRepositorio.findAll();
    }

    public serviciosVariosEntidad obtenerSolicitudPorId(Long id) {
        Optional<serviciosVariosEntidad> servicioVario = serviciosVariosRepositorio.findById(Math.toIntExact(id));
        return servicioVario.orElse(null);
    }

    public serviciosVariosEntidad create(serviciosVariosEntidad serviciosVario, String subject) {
        // Obtener el objeto usuarioresidenteEntidad relacionado
        usuarioresidenteEntidad usuarioResidente = serviciosVario.getFk();

        // Obtener el correo electrónico del usuario residente
        String correoElectronico = usuarioResidente.getCorreoElectronico();

        // Enviar correo electrónico
        emailService.sendEmail(usuarioResidente.getId(), subject);

        return serviciosVariosRepositorio.save(serviciosVario);
    }


    public void eliminarSolicitud(Long id) {
        serviciosVariosRepositorio.deleteById(Math.toIntExact(id));
    }
}