package wax.wax.Infraestructura.output.persistence.entidades;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "serviciosvarios")
public class serviciosVariosEntidad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_servicio", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_id", nullable = false)
    private usuarioresidenteEntidad fk;

    @Column(name = "num", nullable = false)
    private Integer num;

    @Column(name = "correo", nullable = false, length = Integer.MAX_VALUE)
    private String correo;

    @Column(name = "tipo_servicio", nullable = false, length = Integer.MAX_VALUE)
    private String tipoServicio;

    @Column(name = "fecha_solicitud", nullable = false)
    private LocalDate fechaSolicitud;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_estado", nullable = false)
    private estadoservicioEntidad fkEstado;

    @Column(name = "fecha_respuesta")
    private LocalDate fechaRespuesta;

}