package wax.wax.Infraestructura.output.persistence.entidades;



import jakarta.persistence.*;

import java.util.LinkedHashSet;
import java.util.Set;


@Entity
@Table(name = "tiposolicitud")
public class tipoSolicitudEntidad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_tiposolicitud", nullable = false)
    private Integer id;

    @Column(name = "tipo_solicitud", nullable = false, length = Integer.MAX_VALUE)
    private String tipoSolicitud;

    @OneToMany(mappedBy = "fkTiposolicitud")
    private Set<solicitudesEntidad> solicitudes = new LinkedHashSet<>();

}