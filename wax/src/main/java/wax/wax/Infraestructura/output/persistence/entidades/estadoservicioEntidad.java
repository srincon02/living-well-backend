package wax.wax.Infraestructura.output.persistence.entidades;

import jakarta.persistence.*;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "estado_serviciov")
public class estadoservicioEntidad {

    @Id
    @Column(name = "pk_estadoservicio", nullable = false, length = Integer.MAX_VALUE)
    private String id;

    @Column(name = "id_estadoserv", columnDefinition = "not null")
    private int idEstadoserv;


    @OneToMany(mappedBy = "fkEstado")
    private Set<serviciosVariosEntidad> serviciosvarios = new LinkedHashSet<>();

}