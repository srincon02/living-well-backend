package wax.wax.Infraestructura.output.persistence.entidades;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.Date;

@Entity
@Table(name = "solicitudes")
public class solicitudesEntidad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pk_solicitud", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_id", nullable = false)
    private usuarioresidenteEntidad fk;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fk_tiposolicitud", nullable = false)
    private tipoSolicitudEntidad fkTiposolicitud;

    @NotNull
    @Column(name = "num", columnDefinition = "int4[] not null")
    private Integer num;

    @NotNull
    @Column(name = "correo", columnDefinition = "varchar[] not null")
    private String correo;

    @NotNull
    @Column(name = "fecha_rad", columnDefinition = "date[](13) not null")
    private Date fechaRad;

    @NotNull
    @Column(name = "fecha_sol", columnDefinition = "date[](13)")
    private Date fechaSol;

    @NotNull
    @Column(name = "radicado", columnDefinition = "varchar[] not null")
    private String radicado;

    @NotNull
    @Column(name = "descripcion", columnDefinition = "varchar[] not null")
    private String descripcion;
}